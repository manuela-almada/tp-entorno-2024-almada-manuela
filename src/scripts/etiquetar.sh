#!/bin/bash

# Este script trabaja sobre archivos de la carpeta actual que terminan en .jpg,
# clasificándolos usando YOLO:
#
# Debe crearse un archivo con el mismo nombre que la imagen, pero extensión .tag
# donde se guardan las etiquetas. Por ejemplo, un archivo .tag podría tener:

# 2 persons, 1 potteid plant, 1 laptop, 2 books
#
# Asegúrese de devolver un valor de salida acorde a la situación.


# Cambia a la carpeta de imágenes
cd ../../imagenes || { echo "No se pudo cambiar a la carpeta de imágenes"; exit 1; }

# Procesa cada archivo .jpg
for i in *.jpg; do
    if [[ -s "$i" ]]; then
        nombre=$(basename "$i" .jpg)
        nombre2="${nombre}.tag"
        echo "Procesando $i..."

        # Llama a YOLO para hacer la predicción
        yolo predict source="$i" > var

        # Procesa la salida para generar el archivo .tag
        grep "x" var | awk '{for (i=5; i<NF; i++) printf $i " "; print "" }' | grep -o '[a-zA-Z ]*' | awk NF | tr '\n' ',' | sed 's/^ //' | sed 's/,$//' > "$nombre2"

        echo "Etiquetas guardadas en $nombre2"
    else
        echo "No se encontró el archivo $i"
    fi
done

cd - || exit 1

