#!/bin/bash 
#Este script muestra información sobre las imagenes descargadas.
cd ../../imagenes
for imagen in *.jpg ; do
	nom=$(basename "$imagen" .jpg)
	echo -------------------------------------------------------------------------------------------------------------------------
	echo -e "Nombre de la imagen:\n$imagen"
	du -h $imagen | awk '{print "Tamaño:" $1}'  #Muestra los tamaños de archivo en un formato legible por humanos
	stat $imagen | grep Creación | awk '{print "Fecha y hora de descarga:"  $2,$3}'
	cat "${nom}.tag" &>/dev/null
	x=$?
	if [[ $x == 0 ]]
	then
		var=$(cat "${nom}.tag") 
		echo -e "Etiquetas de la imagen:$var"
		
	else
			
		echo "Las etiquetas no fueron encontradas. Algo falló" 
	fi

	#echo $nom2
	#cat "{$nom}.tag"
done
 
