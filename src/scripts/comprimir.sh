#!/bin/bash

# Este script comprime todo el contenido de la carpeta actual y además, genera
# una suma de verificación del archivo resultante en un archivo separado.

cd ../../imagenes

tar -czvf archivos_comprimidos.tar.gz *
#Crea un nuevo archivo, comprime uzando gzip, muestra el progreso y especificamos el nombre de salida del archivo.
echo "Archivo comprimido con éxito"

sha256sum archivos_comprimidos.tar.gz > checksum.txt
echo "La suma de verificación se ha realizado con éxito"

#echo Módulo de compresión no implementado. && exit 1
