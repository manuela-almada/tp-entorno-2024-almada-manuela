#!/bin/bash

# Este script simplemente debe chequear que haya conexión a internet.
# Asegúrese de retornar un valor de salida acorde a la situación.
# Puede que necesite modificar el archivo Dockerfile.

#echo Chequeo de Internet no implementado. && exit 1
ping -c google.com &>/dev/null
X=$?
ping -c https://www.facebook.com/?locale=es_LA &>/dev/null
Y=$?
ping -c https://www.wikipedia.org/ &>/dev/null
Z=$?
if [[ $X==0 || $Y==0 || $Z==0 ]]
then 
	echo Hay Internet
	exit 0
else
	echo No Hay Internet
	exit 1
fi

