#!/bin/bash

# Este script debe descargar una sola imagen desde internet en la carpeta actual.
# Puede recibir un argumento opcional indicando la clase de la imagen.
# El nombre del archivo deberá ser su suma de verificación y debe terminar en .jpg
# Asegúrese de devolver un valor de salida acorde a la situación.

URL0=https://random-image-pepebigotes.vercel.app/api/random-image
URL1=https://loremflickr.com/512/512/
URL2=https://picsum.photos/512

random=$(($RANDOM%3))
case $random in
	0) curl -o "imagen" $URL0
	;;
	1) curl -o "imagen" $URL1
	;;
	2) curl -o "imagen" $URL2
	;;	
esac

#random="URL $(($RANDOM%3))"

#curl -o "imagen" $random

#echo "URL""$(($RANDOM%3))" 

num=$(sha256sum "imagen" | awk '{print $1}')

num2="${num}.jpg"

mv "imagen" "$num2"

#echo Descarga de imágenes de internet no implementada. && exit 1


