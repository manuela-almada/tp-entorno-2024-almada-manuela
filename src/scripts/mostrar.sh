#!/bin/bash

# Este script es un programa interactivo que no recibe argumentos.
# Debe preguntarle al usuario que etiqueta desea buscar y mostrar por
# pantalla todas las imágenes que tengan esa etiqueta.
read -p 'Ingrese una etiqueta : ' etiqueta
cd ../../imagenes 
for j in *.tag
do
	var=$(
	awk -v eti="$etiqueta" -F "," '{
   for (i=1; i<=NF; i++) {
	gsub(/^ +| +$/, "", $i);
        if ($i == eti) {
            var=FILENAME; sub(/\.tag$/, "", var); print var ".jpg"
                    exit;
	  }
    }
}' $j)

echo $var
if [[ $var != ''  ]] 
then
	jp2a --colors  $var
fi

done

