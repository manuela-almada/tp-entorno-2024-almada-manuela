# Trabajo Práctico de Entorno de Programación (2024)

Este repositorio es un trabajo práctico para la materia "Entorno de
Programación" de la carrera T.U.I.A. en F.C.E.I.A. (U.N.R.).

Aquí puede leerse el [enunciado](docs/enunciado.md) del trabajo.

## Instrucciones
Script internet.sh: a traves del comando ping junto con distintas URL, vamos guardando el exitstatus de c/una y luego si todas se ejecutaron correctamente nos dira que hay internest, de lo contrario nos dira que no.

Script descargar.sh: propusimos 3 URL distintas para descargar imagenes, a partir del uso del case c/vez que se ejecute selecciona una de estas URL de forma aleatoria, luego con el comando curl lo descarga. Por ultimo realizamos  una suma de verificación con el comando sha256sum y movemos la imagen descargada a un archivo cuyo nombre es dicha suma terminada en ".jpg".

Script etiquetar.sh: comenzamos ubicandonos en el directorio imagenes recorremos todas las imagenes descargadas con un for que busque los archivos terminados en ".jpg", comprobamos si dichos archivos existen, luego utilizamos yolo para generar las etiquetas, con una serie de comandos conseguimos quedarnos con las etiquetas unicamente, con el formato etiqueta1, etiqueta2, .... Y las redireccionamos a un archivo con el mismo nombre que la imagen solo que en vez de tener extensión .jpg tiene extensión .tag

Script mostrar.sh: pedimos que se ingrese una etiqueta a buscar, luego con el comando awk logramos obtener las imagenes que coincien con esa etiqueta. Por ultimo con el comando jp2a las imprimimos en pantalla.

Script funcionextra.sh: recorremos las imagenes (archivos terminados en .jpg) guardadas en el directorio imagenes, con el comando du logramos obtener su tamaño. Con el comando stat obtenemos informacion del archivo pero unicamente tomamos su fecha y hora de descarga. Imprimmimos todo por pantalla y por ultimo mostramos las etiquetas asociadas a la imagen (en el caso de que esta se haya descargado y generado sus etiquetas con exito)  

   

### Dependencias

Es necesario tener instalados `docker` y `docker buildx` para poder ejecutar
este programa. En distribuciones basadas en Ubuntu esto puede conseguirse así:
```bash
sudo apt update
sudo apt install docker.io docker-buildx
```

Luego será necesario habilitar el servicio de contenedores de docker:
```bash
sudo systemctl enable docker
sudo systemctl start docker
```

También puede ser de utilidad agregar al usuario actual al grupo `docker`:
```bash
sudo usermod -aG docker $USER
```
Para que este cambio surja efecto, es necesario reiniciar la sesión.

### Ejecución

Para poder utilizar el programa primero debe construir el contendor:
```bash
docker buildx build -t entorno .
```

Luego puede ejecutarse el contenedor con el siguiente comando:
```bash
docker run -it entorno
```

Tambien puede correrse el programa fuera del contenedor:
```bash
./src/main.sh
```

## Integrantes

* Ricci Valentino.
* Almada Manuela.
