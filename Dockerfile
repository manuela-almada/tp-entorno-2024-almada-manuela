# syntax = edrevo/dockerfile-plus
INCLUDE+ .Dockerfile.base

# Instalar los programas necesarios
#Instalacion comando ping
RUN apt install -y iputils-ping
#Instalacion comando curl
RUN apt-get update && apt-get install -y curl
#Instalacion comando jp2a
RUN apt-get update 
RUN apt-get install -y jp2a imagemagick

# Configuracion de la aplicación
ENV TERM=xterm
ENV COLORTERM=24bit
COPY ["src/", "/app/"]
WORKDIR /app
ENTRYPOINT ["/app/main.sh"]
